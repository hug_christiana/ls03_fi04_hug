package controller;

import model.User;
import model.persistance.IPersistance;
import model.persistance.IUserPersistance;

/**
 * controller for users
 * @author Clara Zufall
 * TODO implement this class
 */
public class UserController {

	private IUserPersistance userPersistance;
	
	public UserController(IPersistance persistance) {
		this.userPersistance = persistance.getUserPersistance();
		}
	
	public void createUser(User user) {
		
		String username = user.getLoginname();
		
		if (userPersistance.readUser(username) == null);
		{
			userPersistance.createUser(user);
		}
	}
	
	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
	 * @param username eindeutige Loginname
	 * @param passwort das richtige Passwort
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	public User readUser(String username, String passwort) {
		
		return userPersistance.readUser(username);
	
	}
	
	public void changeUser(User user) {
		String username = user.getLoginname();
		
		if (userPersistance.readUser(username) != null);
		{
			userPersistance.updateUser(user);
		}
		
	}
	
	public void deleteUser(User user) {
		String username = user.getLoginname();
		
		if (userPersistance.readUser(username) != null);
		{
			userPersistance.deleteUser(user);
		}
	}
	
	public boolean checkPassword(String username, String passwort) {
		
		if (userPersistance.readUser(username).getPassword().equals(passwort)) {
			return true;
		}
		else {
			return false;
		}
	}
}
