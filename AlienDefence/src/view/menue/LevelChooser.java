package view.menue;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import model.Level;
import model.User;
import view.game.GameGUI;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;
	private AlienDefenceController alienDefenceController;
	private User user;
	private boolean startGame;
	private boolean getHighscore;
	private boolean levelSelected;


	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 * @param alienDefenceController 
	 * @param alienDefenceController 
	 * @wbp.parser.constructor
	 */
	public LevelChooser(LevelController lvlControl, LeveldesignWindow leveldesignWindow, AlienDefenceController alienDefenceController, boolean startGame, boolean getHighscore) {
		setBackground(Color.BLACK);
		this.lvlControl = lvlControl;
		this.leveldesignWindow = leveldesignWindow;
		this.alienDefenceController = alienDefenceController;
		this.startGame = startGame;
		this.getHighscore = getHighscore;
		this.levelSelected = false;

		setLayout(new BorderLayout());

		JPanel pnlButtons = new JPanel();
		add(pnlButtons, BorderLayout.SOUTH);

		JButton btnNewLevel = new JButton("Neues Level");
		btnNewLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnNewLevel_Clicked();
			}
		});
		pnlButtons.add(btnNewLevel);

		JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
		btnUpdateLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdateLevel_Clicked();
			}
		});
		pnlButtons.add(btnUpdateLevel);

		JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
		btnDeleteLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteLevel_Clicked();
			}
		});
		pnlButtons.add(btnDeleteLevel);
		
		JButton btnStartGame = new JButton("Spielen");
		btnStartGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStartGame_Clicked();
			}
		});
		pnlButtons.add(btnStartGame);
		
		if (startGame == false)
		{
			btnStartGame.setVisible(false);
		}
		
		JButton btnHighscore = new JButton("Highscore");
		btnHighscore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnHighscore_Clicked();
			}
		});
		pnlButtons.add(btnHighscore);
				
		if (getHighscore == false){
			btnHighscore.setVisible(false);
		}
		
		if (startGame==true || getHighscore == true)
		{
			btnNewLevel.setVisible(false);
			btnUpdateLevel.setVisible(false);
			btnDeleteLevel.setVisible(false);
		}

		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		lblLevelauswahl.setForeground(Color.GREEN);
		lblLevelauswahl.setFont(new Font("Arial", Font.BOLD, 18));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLevelauswahl, BorderLayout.NORTH);

		JScrollPane spnLevels = new JScrollPane();
//		spnLevels.addMouseListener(new MouseAdapter() {
//			@Override
//			public void mouseClicked(MouseEvent e) {
//				levelSelected = true;
//			}
//		});
		add(spnLevels, BorderLayout.CENTER);

		tblLevels = new JTable();
		tblLevels.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				levelSelected = true;
			}
		});
		tblLevels.getTableHeader().setForeground(Color.green);
		tblLevels.getTableHeader().setBackground(Color.black);
		tblLevels.setForeground(new Color(255, 200, 0));
		tblLevels.setBackground(Color.BLACK);
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spnLevels.setViewportView(tblLevels);

		this.updateTableData();
	}
	
	public LevelChooser(LevelController lvlControl, LeveldesignWindow leveldesignWindow, AlienDefenceController alienDefenceController, User user, boolean startGame, boolean getHighscore) 
	{
		this(lvlControl, leveldesignWindow, alienDefenceController, startGame, getHighscore);
		this.user = user;
	}

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		if (levelSelected == true)
		{
			int level_id = Integer
					.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
			this.leveldesignWindow.startLevelEditor(level_id);
		}
	}

	public void btnDeleteLevel_Clicked() {
		if (levelSelected == true)
		{
			int level_id = Integer
					.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
			this.lvlControl.deleteLevel(level_id);
			this.updateTableData();
		}
	}
	
	public void btnStartGame_Clicked()
	{
		if (levelSelected == true)
		{
			int level_id = Integer
					.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
			Thread t = new Thread("GameThread") {
				@Override
				public void run() {
	
					GameController gameController = alienDefenceController.startGame(lvlControl.readLevel(level_id), user);
					new GameGUI(gameController).start();
				}
			};
			t.start(); 
		}
	}
	
	public void btnHighscore_Clicked()
	{
		
		if (levelSelected == true)
		{
			int level_id = Integer
					.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
			new Highscore(alienDefenceController.getAttemptController(), level_id);
		}
	}
	
}
