package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

public class MyForm extends JFrame
{

	private JPanel contentPane;
	private JTextField tfdEnterText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					MyForm frame = new MyForm();
					frame.setVisible(true);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MyForm()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 484, 673);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblText = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lblText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblText.setHorizontalAlignment(SwingConstants.CENTER);
		lblText.setBounds(10, 29, 443, 26);
		contentPane.add(lblText);
		
		JButton btnRedBackground = new JButton("Rot");
		btnRedBackground.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonRedBackground_clicked();
			}
		});
		btnRedBackground.setBounds(15, 93, 136, 23);
		contentPane.add(btnRedBackground);
		
		JButton btnGreenBackground = new JButton("Gr\u00FCn");
		btnGreenBackground.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonGreenBackground_clicked();
			}
		});
		btnGreenBackground.setBounds(166, 93, 136, 23);
		contentPane.add(btnGreenBackground);
		
		JButton btnBlueBackground = new JButton("Blau");
		btnBlueBackground.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonBlueBackground_clicked();
			}
		});
		btnBlueBackground.setBounds(317, 93, 136, 23);
		contentPane.add(btnBlueBackground);
		
		JButton btnYellowBackground = new JButton("Gelb");
		btnYellowBackground.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonYellowBackground_clicked();
			}
		});
		btnYellowBackground.setBounds(14, 137, 137, 23);
		contentPane.add(btnYellowBackground);
		
		JButton btnStandardBackground = new JButton("Standardfarbe");
		btnStandardBackground.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonStandardBackground_clicked();
			}
		});
		btnStandardBackground.setBounds(165, 137, 137, 23);
		contentPane.add(btnStandardBackground);
		
		JButton btnColorPicker = new JButton("Farbe ausw\u00E4hlen");
		btnColorPicker.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonColorPicker_clicked();
			}
		});
		btnColorPicker.setBounds(316, 137, 137, 23);
		contentPane.add(btnColorPicker);
		
		JLabel lblChangeBackgroundColor = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblChangeBackgroundColor.setBounds(33, 66, 394, 14);
		contentPane.add(lblChangeBackgroundColor);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblText.setFont(new Font("Arial", Font.PLAIN, 12));
			}
		});
		btnArial.setBounds(15, 213, 136, 23);
		contentPane.add(btnArial);
		
		JButton btnComicSans = new JButton("Comic Sans MS");
		btnComicSans.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblText.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
			}
		});
		btnComicSans.setBounds(166, 213, 136, 23);
		contentPane.add(btnComicSans);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblText.setFont(new Font("Courier New", Font.PLAIN, 12));
			}
		});
		btnCourierNew.setBounds(317, 213, 136, 23);
		contentPane.add(btnCourierNew);
		
		JLabel lblFormatText = new JLabel("Aufgabe 2: Text formatieren");
		lblFormatText.setBounds(33, 185, 420, 14);
		contentPane.add(lblFormatText);
		
		tfdEnterText = new JTextField();
		tfdEnterText.setText("Hier bitte Text eingeben.");
		tfdEnterText.setBounds(15, 268, 438, 20);
		contentPane.add(tfdEnterText);
		tfdEnterText.setColumns(10);
		
		JButton btnEnterText = new JButton("Ins Label schreiben");
		btnEnterText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblText.setText(tfdEnterText.getText());
			}
		});
		btnEnterText.setBounds(42, 310, 171, 23);
		contentPane.add(btnEnterText);
		
		JButton btnDeleteText = new JButton("Text aus Label l\u00F6schen");
		btnDeleteText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblText.setText(null);
			}
		});
		btnDeleteText.setBounds(255, 310, 171, 23);
		contentPane.add(btnDeleteText);
		
		JButton btnRedForeground = new JButton("Rot");
		btnRedForeground.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblText.setForeground(Color.RED);
			}
		});
		btnRedForeground.setBounds(15, 385, 136, 23);
		contentPane.add(btnRedForeground);
		
		JButton btnBlueForeground = new JButton("Blau");
		btnBlueForeground.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblText.setForeground(Color.BLUE);
			}
		});
		btnBlueForeground.setBounds(166, 385, 136, 23);
		contentPane.add(btnBlueForeground);
		
		JButton btnBlackForeground = new JButton("Schwarz");
		btnBlackForeground.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblText.setForeground(Color.BLACK);
			}
		});
		btnBlackForeground.setBounds(317, 385, 136, 23);
		contentPane.add(btnBlackForeground);
		
		JLabel lblFontColor = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblFontColor.setBounds(33, 362, 420, 14);
		contentPane.add(lblFontColor);
		
		JButton btnIncreaseFontSize = new JButton("+");
		btnIncreaseFontSize.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int groesse = lblText.getFont().getSize();
				String font = lblText.getFont().getName();
				lblText.setFont(new Font(font, Font.PLAIN, groesse + 1));
			}
		});
		btnIncreaseFontSize.setBounds(42, 461, 171, 23);
		contentPane.add(btnIncreaseFontSize);
		
		JButton btnDecreaseFontSize = new JButton("-");
		btnDecreaseFontSize.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int groesse = lblText.getFont().getSize();
				String font = lblText.getFont().getName();
				lblText.setFont(new Font(font, Font.PLAIN, groesse - 1));
			}
		});
		btnDecreaseFontSize.setBounds(255, 461, 171, 23);
		contentPane.add(btnDecreaseFontSize);
		
		JLabel lblFontSize = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe \u00E4ndern");
		lblFontSize.setBounds(33, 436, 420, 14);
		contentPane.add(lblFontSize);
		
		JLabel lblAlignment = new JLabel("Aufgabe 5: Textausrichtung");
		lblAlignment.setBounds(33, 507, 420, 14);
		contentPane.add(lblAlignment);
		
		JButton btnAlignLeft = new JButton("linksb\u00FCndig");
		btnAlignLeft.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblText.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnAlignLeft.setBounds(15, 532, 136, 23);
		contentPane.add(btnAlignLeft);
		
		JButton btnAlignCenter = new JButton("zentriert");
		btnAlignCenter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblText.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnAlignCenter.setBounds(166, 532, 136, 23);
		contentPane.add(btnAlignCenter);
		
		JButton btnAlignRight = new JButton("rechtsb\u00FCndig");
		btnAlignRight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblText.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnAlignRight.setBounds(317, 532, 136, 23);
		contentPane.add(btnAlignRight);
		
		JLabel lblExit = new JLabel("Aufgabe 6: Programm beenden");
		lblExit.setBounds(33, 577, 420, 14);
		contentPane.add(lblExit);
		
		JButton btnExit = new JButton("Programm beenden");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(1);
			}
		});
		btnExit.setBounds(15, 602, 438, 23);
		contentPane.add(btnExit);
	}

	public void buttonRedBackground_clicked()
	{
		this.contentPane.setBackground(Color.RED);
		
	}
	
	public void buttonGreenBackground_clicked()
	{
		this.contentPane.setBackground(Color.GREEN);
		
	}
	
	public void buttonBlueBackground_clicked()
	{
		this.contentPane.setBackground(Color.BLUE);
		
	}
	
	public void buttonYellowBackground_clicked()
	{
		this.contentPane.setBackground(Color.YELLOW);
		
	}
	
	public void buttonStandardBackground_clicked()
	{
		this.contentPane.setBackground(new Color(0xEEEEEE));
		
	}
	
	public void buttonColorPicker_clicked()
	{
		this.contentPane.setBackground(JColorChooser.showDialog(null, "Farbauswahl", null));
		
	}
}
